package springApp.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import springApp.pojos.Employee;
import springApp.pojos.Role;
import springApp.repository.EmployeeRepository;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
public class HibernateUserService implements UserDetailsService {

    private EmployeeRepository employeeRepository;

    @Autowired
    public HibernateUserService(EmployeeRepository userRepository) {
        this.employeeRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<Employee> employeeOpt = employeeRepository.findById(username);
        if (!employeeOpt.isPresent()) {
            throw new UsernameNotFoundException("Username not found");
        }
        Employee employee = employeeOpt.get();
        Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
        for (Role role : employee.getRoles()) {
            grantedAuthorities.add(new SimpleGrantedAuthority(role.getName()));
        }
        return new User(employee.getLogin(), employee.getPassword(), grantedAuthorities);
    }
}
