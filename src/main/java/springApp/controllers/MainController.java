package springApp.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.multipart.MultipartFile;
import springApp.pojos.Artifact;
import springApp.pojos.Employee;
import springApp.pojos.Review;
import springApp.pojos.Session;
import springApp.repository.ArtifactRepository;
import springApp.repository.EmployeeRepository;
import springApp.repository.ReviewRepository;
import springApp.repository.SessionRepository;
import springApp.storage.StorageResponse;
import springApp.storage.StorageService;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.security.Principal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Controller
public class MainController {

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private ReviewRepository reviewRepository;

    @Autowired
    private SessionRepository sessionRepository;

    @Autowired
    private ArtifactRepository artifactRepository;

    @Autowired
    private StorageService storageService;

//    @GetMapping("/artifacts")
//    public String artifacts() {
//        return "artifacts";
//    }

    @GetMapping("/")
    public String index() {
        return "index";
    }

    @GetMapping("/login")
    public String login(Principal principal) {
        if (principal != null && ((Authentication) principal).isAuthenticated()) {
            return "redirect:/";
        }
        return "login";
    }

    @GetMapping("/session/{sessionId}")
    public String session(@PathVariable String sessionId,
                          Model model, Principal principal){
        return "session";
    }

    @GetMapping("/createSession")
    public String createSession(Model model, Principal principal) {
        Optional<Employee> user = employeeRepository.findById(principal.getName());
        if(user.isPresent()){
            model.addAttribute("reviews", user.get().getCreatedReviews().stream()
                    .filter(review -> review.getRevStatus().equalsIgnoreCase("OPEN"))
                    .collect(Collectors.toSet()));
            model.addAttribute("oldReviews", user.get().getCreatedReviews().stream()
                    .filter(review -> review.getRevStatus().equalsIgnoreCase("CLOSED"))
                    .collect(Collectors.toSet()));
        }
        return "createSession";
    }

    @PostMapping("/createSession")
    public String creatingSession(@RequestParam String reviewId,
                                  @RequestParam String submit,
                                  Model model, Principal principal) {
        Long id = Long.parseLong(reviewId);
        Optional<Review> review = reviewRepository.findById(id);
        if(submit.equalsIgnoreCase("error")){
            return createSession(model, principal);
        }
        if (review.isPresent()) {
            if(submit.equalsIgnoreCase("add")) {
                if (review.get().getRevSessions().stream()
                        .map(Session::getSessStatus)
                        .filter(status -> status.equalsIgnoreCase("OPEN"))
                        .count() > 0) {
                    model.addAttribute("message", "There is already open session for this review");
                    return createSession(model, principal);
                }

                Optional<Employee> user = employeeRepository.findById(principal.getName());
                if (user.isPresent() && review.get().getRevAuthor().equals(user.get())) {
                    model.addAttribute("reviewId", reviewId);
                    return "creatingSession";
                }
            }else{
                review.get().getRevSessions().forEach(session -> {session.setSessStatus("CLOSED");
                                                                    session.setSessInvitations(Collections.emptySet());});
                review.get().setRevStatus("CLOSED");
                review.get().setRevInvitations(Collections.emptySet());
                reviewRepository.save(review.get());
            }
        }
        return createSession(model, principal);
    }

    @PostMapping("/createdSession")
    public String createdSession(@RequestParam String reviewId,
                                 @RequestParam String startTime,
                                 @RequestParam String endTime,
                                 @RequestParam String description,
                                 @RequestParam("sessArtifact") MultipartFile file,
                                 Model model,
                                 Principal principal) throws IOException {

        List<String> errors = new ArrayList<>();

        Optional<Employee> authorEmployee = employeeRepository.findById(principal.getName());
        if (!authorEmployee.isPresent()) {
            errors.add("Cannot retrieve author");
        }

        Long id = Long.parseLong(reviewId);
        Optional<Review> reviewOpt = reviewRepository.findById(id);
        if (!reviewOpt.isPresent()) {
            errors.add("Cannot find parent review");
        }

        Review review = reviewOpt.get();

        String filePath;
        String fileType;

        if (file.isEmpty()) {
            filePath = review.getRevArtifact().getSource();
            fileType = review.getRevArtifact().getType();
        } else {
            StorageResponse storageResponse = storageService.store(file);
            if (storageResponse.getStatus() != HttpStatus.OK) {
                errors.add(storageResponse.getMessage());
            }
            filePath = storageResponse.getPath();
            fileType = storageResponse.getType();
        }

        if (errors.size() > 0) {
            model.addAttribute("errors", errors);
            return creatingSession(reviewId, "error", model, principal);
        }

        Optional<Artifact> existingArtifact = artifactRepository.findBySource(filePath);
        Artifact artifact = existingArtifact
                .orElseGet(() -> artifactRepository.save(new Artifact(filePath, fileType)));

        Employee author = authorEmployee.get();

        Session session = new Session();
        session.setDescription(description);
        session.setSessArtifact(artifact);
        session.setSessAuthor(author);
        session.setSessInvitations(new HashSet<>(review.getRevEmployees()));
        if (session.getSessInvitations() != null) {
            session.getSessInvitations().addAll(review.getRevInvitations());
        } else {
            session.setSessInvitations(new HashSet<>(review.getRevInvitations()));
        }
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd'T'HH:mm");

            session.setStart_time(new Timestamp(dateFormat.parse(startTime).getTime()));
            session.setEnd_time(new Timestamp(dateFormat.parse(endTime).getTime()));
        } catch (Exception e) {
            errors.add("Wrong format of time, required dd-mm-yyyyTHH:mm");
            model.addAttribute("errors", errors);
            return creatingSession(reviewId, "error", model, principal);
        }
        session.setSessStatus("OPEN");
        session.setSessReview(review);
        Session savedSession = sessionRepository.save(session);

        review.setRevArtifact(artifact);
        review.getRevSessions().add(savedSession);
        reviewRepository.save(review);

        model.addAttribute("message", "Succesfully added new review and sent invitations!");
        return "success";
    }

    @GetMapping("/activeSessions")
    public String activeSessions(Model model, Principal principal) {
        Optional<Employee> user = employeeRepository.findById(principal.getName());
        if (user.isPresent()) {
            model.addAttribute("ownedSessions", user.get().getCreatedSessions()
                    .stream()
                    .filter(session -> session.getSessStatus().equalsIgnoreCase("OPEN"))
                    .collect(Collectors.toSet()));
            model.addAttribute("sessions", user.get().getSessions()
                    .stream()
                    .filter(session -> session.getSessStatus().equalsIgnoreCase("OPEN"))
                    .collect(Collectors.toSet()));
        }
        return "activeSessions";
    }

    @PostMapping("/activeSessions")
    public String activeSessions(@RequestParam String sessId,
                                 Model model, Principal principal) {
        Optional<Employee> user = employeeRepository.findById(principal.getName());
        Optional<Session> session = sessionRepository.findById(Long.parseLong(sessId));
        if(user.isPresent() && session.isPresent()){
            session.get().setSessStatus("CLOSED");
            session.get().setSessInvitations(Collections.emptySet());
            sessionRepository.save(session.get());
        }
        return activeSessions(model, principal);
    }

    @GetMapping("/createReview")
    public String createReview(Model model, Principal principal) {
        Employee user = employeeRepository.findById(principal.getName()).orElse(new Employee());
        List<Employee> employees = employeeRepository.findAll();
        employees.remove(user);
        model.addAttribute("employees", employees);
        return "createReview";
    }

    @ExceptionHandler(MaxUploadSizeExceededException.class)
    public String handleSizeExceededException(HttpServletRequest request, Exception ex, Model model, Principal principal) {
        List<String> errors = new ArrayList<>();
        errors.add(ex.getMessage());
        return createReview(model, principal);
    }

    @PostMapping("/createReview")
    public String createdReview(@RequestParam(value = "reviewers", required = false) String[] employeeLogins,
                                @RequestParam("description") String description,
                                @RequestParam("revArtifact") MultipartFile file,
                                Model model,
                                Principal principal) throws IOException, MaxUploadSizeExceededException {
        StorageResponse storageResponse = storageService.store(file);
        List<String> errors = new ArrayList<>();
        if (storageResponse.getStatus() != HttpStatus.OK) {
            errors.add(storageResponse.getMessage());
        }

        Optional<Employee> authorEmployee = employeeRepository.findById(principal.getName());
        if (!authorEmployee.isPresent()) {
            errors.add("Cannot retrieve author");
        }

        if (employeeLogins == null) {
            errors.add("No employees added to review");
        }

        if (errors.size() > 0) {
            model.addAttribute("errors", errors);
            return createReview(model, principal);
        }

        Set<Employee> invited = Arrays.stream(employeeLogins)
                .map(login -> employeeRepository.findById(login))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toSet());

        if (employeeLogins.length != invited.size()) {
            errors.add("Cannot find all employees in database!");
            model.addAttribute("errors", errors);
            return createReview(model, principal);
        }

        Optional<Artifact> existingArtifact = artifactRepository.findBySource(storageResponse.getPath());
        Artifact artifact = existingArtifact
                .orElseGet(() -> artifactRepository.save(new Artifact(storageResponse.getPath(), storageResponse.getType())));

        Employee author = authorEmployee.get();
        Review review = new Review();
        review.setDescription(description);
        review.setRevArtifact(artifact);
        review.setRevAuthor(author);
        review.setRevInvitations(invited);
        review.setRevStatus("OPEN");
        reviewRepository.save(review);

        model.addAttribute("message", "Succesfully added new review and sent invitations!");
        return "success";
    }

    @GetMapping("/invitations")
    public String invitations(Model model, Principal principal) {
        Optional<Employee> user = employeeRepository.findById(principal.getName());
        if (user.isPresent()) {
            model.addAttribute("reviewInvitations", user.get().getReviewInvitations());
            model.addAttribute("sessionInvitations", user.get().getSessionInvitations());
        }
        return "invitations";
    }

    @PostMapping("/invitations")
    public String invitationsHandle(@RequestParam("submit") String choice,
                                    @RequestParam(value = "reviewId", required = false) String reviewId,
                                    @RequestParam(value = "sessionId", required = false) String sessionId,
                                    Model model, Principal principal) {
        if (reviewId != null && !reviewId.isEmpty()) {
            if (!handleReviewInvitation(Long.parseLong(reviewId), choice, principal)) {
                model.addAttribute("message", "Error when handling invitation");
            }
        }
        if (sessionId != null && !sessionId.isEmpty()) {
            if (!handleSessionInvitation(Long.parseLong(sessionId), choice, principal)) {
                model.addAttribute("message", "Error when handling invitation");
            }
        }

        return invitations(model, principal);
    }

    private boolean handleReviewInvitation(Long reviewId, String choice, Principal principal) {
        Optional<Employee> user = employeeRepository.findById(principal.getName());
        Review rev;
        boolean result = false;
        if (user.isPresent()) {
            Employee employee = user.get();
            Optional<Review> review = reviewRepository.findById(reviewId);
            if (review.isPresent() && review.get().getRevInvitations().contains(employee)) {
                rev = review.get();
                if ("ACCEPT".equalsIgnoreCase(choice)) {
                    if (rev.getRevEmployees() == null) {
                        rev.setRevEmployees(Collections.singleton(employee));
                    } else {
                        rev.getRevEmployees().add(employee);
                    }
                }
                if ("DECLINE".equalsIgnoreCase(choice) || "ACCEPT".equalsIgnoreCase(choice)) {
                    rev.getRevInvitations().remove(employee);
                    reviewRepository.save(rev);
                    result = true;
                }
            }
        }
        return result;
    }

    private boolean handleSessionInvitation(Long sessionId, String choice, Principal principal) {
        Optional<Employee> user = employeeRepository.findById(principal.getName());
        Session sess;
        boolean result = false;
        if (user.isPresent()) {
            Employee employee = user.get();
            Optional<Session> session = sessionRepository.findById(sessionId);
            if (session.isPresent() && session.get().getSessInvitations().contains(employee)) {
                sess = session.get();
                if ("ACCEPT".equalsIgnoreCase(choice)) {
                    if (sess.getSessEmployees() == null) {
                        sess.setSessEmployees(Collections.singleton(employee));
                    } else {
                        sess.getSessEmployees().add(employee);
                    }
                }
                if ("DECLINE".equalsIgnoreCase(choice) || "ACCEPT".equalsIgnoreCase(choice)) {
                    sess.getSessInvitations().remove(employee);
                    sessionRepository.save(sess);
                    result = true;
                }
            }
        }
        return result;
    }
}