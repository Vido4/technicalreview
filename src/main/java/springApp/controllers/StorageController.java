//package springApp.controllers;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.*;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.multipart.MultipartFile;
//import springApp.pojos.Artifact;
//import springApp.repository.ArtifactRepository;
//import springApp.storage.StorageProperties;
//import springApp.storage.StorageResponse;
//import springApp.storage.StorageService;
//import sun.misc.IOUtils;
//
//import java.io.File;
//import java.io.IOException;
//import java.nio.file.Path;
//import java.nio.file.Paths;
//import java.util.Optional;
//
//@RestController
//public class StorageController {
//
//    private final StorageService storageService;
//    private Path rootLocation;
//
//    @Autowired
//    private ArtifactRepository artifactRepository;
//
//    @Autowired
//    public StorageController(StorageService storageService, StorageProperties properties) {
//        this.rootLocation = Paths.get(properties.getLocation());
//        this.storageService = storageService;
//    }
//
//    @PostMapping("/artifacts")
//    public StorageResponse handleFileUpload(@RequestParam("artifact") MultipartFile file)
//            throws IOException {
//        return storageService.store(file);
//    }
//}
