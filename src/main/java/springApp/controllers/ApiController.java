package springApp.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springApp.pojos.*;
import springApp.repository.CommentRepository;
import springApp.repository.EmployeeRepository;
import springApp.repository.SessionRepository;
import springApp.storage.StorageService;

import java.security.Principal;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Date;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@RestController
public class ApiController {

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private SessionRepository sessionRepository;

    @Autowired
    private CommentRepository commentRepository;

    @Autowired
    private StorageService storageService;

    @GetMapping("/api/session/{sessionId}")
    public SessionResponse createSession(@PathVariable String sessionId,
                                         Principal principal) {
        Optional<Employee> user = employeeRepository.findById(principal.getName());
        Optional<Session> session = sessionRepository.findById(Long.parseLong(sessionId));
        SessionResponse sessionResponse = new SessionResponse();
        if (user.isPresent() && session.isPresent()) {
            Artifact artifact = session.get().getSessArtifact();

            sessionResponse.setDescription(session.get().getDescription());
            sessionResponse.setData(storageService.getContent(artifact.getSource(), artifact.getType()));
            sessionResponse.setType(artifact.getType().toLowerCase());
            sessionResponse.setComments(processComments(session.get().getComments()));
        }
        return sessionResponse;
    }

    @PostMapping("/api/session/{sessionId}/comment")
    public SessionResponse addComment(@RequestBody CommentRequest commentRequest,
                                      @PathVariable String sessionId,
                                      Principal principal) {
        Comment comment = new Comment();
        Optional<Employee> user = employeeRepository.findById(principal.getName());
        Optional<Session> currSess = sessionRepository.findById(Long.parseLong(sessionId));
        if (user.isPresent() && currSess.isPresent()) {
            comment.setCommAuthor(user.get());
            comment.setCommDate(new Timestamp(Date.from(Instant.parse(commentRequest.getDate())).getTime()));
            comment.setData(commentRequest.getData());
            comment.setCommSession(currSess.get());
            commentRepository.save(comment);
            return createSession(sessionId, principal);
        }
        return new SessionResponse();
    }

    private Set<CommentResponse> processComments(Set<Comment> comments) {
        Set<CommentResponse> commentResponses = new HashSet<CommentResponse>();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
        for (Comment comment : comments) {
            CommentResponse commentResponse = new CommentResponse();
            commentResponse.setUser(comment.getCommAuthor().getName() + " " + comment.getCommAuthor().getSurname());
            commentResponse.setDate(dateFormat.format(comment.getCommDate()));
            commentResponse.setData(comment.getData());
            commentResponse.setUrl(getEmployeeUrl(comment.getCommAuthor()));
            commentResponses.add(commentResponse);
        }
        return commentResponses;
    }

    private String getEmployeeUrl(Employee e) {
        return "/api/employees/" + e.getLogin();
    }
}
