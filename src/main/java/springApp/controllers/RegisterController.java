package springApp.controllers;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import springApp.errors.ErrorMessages;
import springApp.pojos.Employee;
import springApp.pojos.PasswordContainer;
import springApp.pojos.Role;
import springApp.repository.EmployeeRepository;
import springApp.repository.RoleRepository;

import javax.validation.Valid;
import java.security.Principal;
import java.util.Collections;
import java.util.Optional;

@Controller
@RequestMapping(value = "/register")
public class RegisterController {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private RoleRepository roleRepository;

    @RequestMapping(value = "/generatePassword", method = RequestMethod.GET)
    public String generatePassword() {
        return "register/generatePassword";
    }

    @RequestMapping(value = "/generatePassword", method = RequestMethod.POST)
    public String generatedPassword(String login, Model model) {
        Optional<Employee> employee = employeeRepository.findById(login);
        if (employee.isPresent()) {
            Employee realEmployee = employee.get();
            String tempPassword = RandomStringUtils.randomAlphanumeric(10, 12);
            realEmployee.setPassword(passwordEncoder.encode(tempPassword));
            employeeRepository.save(realEmployee);
            model.addAttribute("tempPassword", tempPassword);
            model.addAttribute("login", realEmployee.getLogin());
            return "register/generatedPassword";
        }
        model.addAttribute("noSuchUser", ErrorMessages.noSuchUser);
        return "register/generatePassword";
    }

    @RequestMapping(value = "/changePassword", method = RequestMethod.GET)
    public String changePassword(PasswordContainer passwordContainer) {
        return "register/changePassword";
    }

    @RequestMapping(value = "/changePassword", method = RequestMethod.POST)
    public String changePassword(@Valid PasswordContainer passwordContainer, BindingResult bindingResult,
                                 Model model, Principal principal) {
        String username = principal.getName();
        Employee user = employeeRepository.findById(username).get();

        validatePassword(bindingResult, passwordContainer, user);

        if (!bindingResult.hasErrors()) {
            user.setPassword(passwordEncoder.encode(passwordContainer.getNewPassword()));
            employeeRepository.save(user);
            return "register/changedPassword";
        }

        return "register/changePassword";
    }

    private void validatePassword(BindingResult bindingResult, PasswordContainer passwordContainer, Employee user) {
        if (!passwordEncoder.matches(passwordContainer.getOldPassword(), user.getPassword())) {
            bindingResult.addError(new FieldError("passwordContainer",
                    "newPassword", ErrorMessages.invalidPassword));
        }

        if (!passwordContainer.getNewPassword().equals(passwordContainer.getNewPasswordRepeat())) {
            bindingResult.addError(new FieldError("passwordContainer",
                    "newPassword", ErrorMessages.unmatchedPassword));
        }
    }

    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public String register(Employee user) {
        return "register/register";
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String registered(@Valid Employee employee, BindingResult bindingResult, Model model) {

        if (employeeRepository.findById(employee.getLogin()).isPresent()) {
            bindingResult.addError(new FieldError("employee", "login", ErrorMessages.existingLogin));
        }
        if (employeeRepository.findByEmail(employee.getLogin()).isPresent()) {
            bindingResult.addError(new FieldError("employee", "email", ErrorMessages.existingEmail));
        }

        if (bindingResult.hasErrors()) {
            return "register/register";
        }

//        String tempPassword = RandomStringUtils.randomAlphanumeric(10, 12);
        String tempPassword = employee.getLogin();//TODO replace it with randomAlphanumeric - for testing purposes only

        registerEmployee(employee, tempPassword);

        model.addAttribute("tempPassword", tempPassword);
        return "register/registered";
    }

    private void registerEmployee(Employee user, String tempPassword) {
        String hashedPassword = passwordEncoder.encode(tempPassword);
        user.setPassword(hashedPassword);
        Role userRole = roleRepository.findByName("ROLE_USER");
        if (userRole == null) {
            userRole = new Role();
            userRole.setName("ROLE_USER");
            roleRepository.save(userRole);
        }
        user.setRoles(Collections.singleton(userRole));
        employeeRepository.save(user);
    }
}
