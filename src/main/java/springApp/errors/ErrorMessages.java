package springApp.errors;

public class ErrorMessages {
    public final static String existingLogin = "User with this login already exists.";
    public final static String existingEmail = "User with this email already exists.";
    public final static String unmatchedPassword = "New passwords don't match.";
    public final static String invalidPassword = "Incorrect old password.";
    public final static String noSuchUser = "User with this login doesn't exist.";
    public final static String noUserRoleFound = "Cannot add new user, please contact administrator";
}
