package springApp.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import springApp.pojos.Artifact;

import java.util.Optional;

@RepositoryRestResource(exported = false)
public interface ArtifactRepository extends CrudRepository<Artifact, Long> {
    Optional<Artifact> findBySource(String source);
}
