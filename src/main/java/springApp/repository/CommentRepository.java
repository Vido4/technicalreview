package springApp.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import springApp.pojos.Comment;

@RepositoryRestResource(exported = false)
public interface CommentRepository extends CrudRepository<Comment, Long> {
}
