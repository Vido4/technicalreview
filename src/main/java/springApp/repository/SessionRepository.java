package springApp.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import springApp.pojos.Session;

@RepositoryRestResource(exported = false)
public interface SessionRepository extends CrudRepository<Session, Long> {
}
