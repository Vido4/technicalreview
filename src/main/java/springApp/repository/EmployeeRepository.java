package springApp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import springApp.pojos.Employee;

import java.util.Optional;

@RepositoryRestResource(exported = false)
public interface EmployeeRepository extends JpaRepository<Employee, String> {
    Optional<Employee> findByEmail(String email);

    // Prevents POST /people and PATCH /people/:id
    @Override
    @RestResource(exported = false)
    Employee save(Employee s);

    // Prevents DELETE /people/:id
    @Override
    @RestResource(exported = false)
    void delete(Employee t);
}
