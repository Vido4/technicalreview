package springApp.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import springApp.pojos.Review;

@RepositoryRestResource(exported = false)
public interface ReviewRepository extends CrudRepository<Review, Long> {

}
