package springApp.pojos;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
public class Comment {
    private Long id;
    private Employee commAuthor;
    private Timestamp commDate;
    private String data;
    private Session commSession;

    @Id
    @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne
    public Employee getCommAuthor() {
        return commAuthor;
    }

    public void setCommAuthor(Employee commAuthor) {
        this.commAuthor = commAuthor;
    }

    @ManyToOne
    public Session getCommSession() {
        return commSession;
    }

    public void setCommSession(Session commSession) {
        this.commSession = commSession;
    }

    @Column(nullable = false)
    public Timestamp getCommDate() {
        return commDate;
    }

    public void setCommDate(Timestamp commDate) {
        this.commDate = commDate;
    }

    @Column(nullable = false)
    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
