package springApp.pojos;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class PasswordContainer {
    private String oldPassword;
    private String newPassword;
    private String newPasswordRepeat;

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword.trim();
    }

    @NotNull
    @Size(min = 4, max = 24, message = "{password.size}")
    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword.trim();
    }

    public String getNewPasswordRepeat() {
        return newPasswordRepeat;
    }

    public void setNewPasswordRepeat(String newPasswordRepeat) {
        this.newPasswordRepeat = newPasswordRepeat.trim();
    }
}
