package springApp.pojos;

import javax.persistence.*;
import java.util.Set;

@Entity
public class Review {
    private Long id;
    private String revStatus;
    private String description;
    private Employee revAuthor;
    private Artifact revArtifact;
    private Set<Employee> revEmployees;
    private Set<Employee> revInvitations;
    private Set<Session> revSessions;

    @Id
    @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(nullable = false)
    public String getRevStatus() {
        return revStatus;
    }

    public void setRevStatus(String revStatus) {
        this.revStatus = revStatus;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @ManyToOne
    @JoinColumn(nullable = false)
    public Employee getRevAuthor() {
        return revAuthor;
    }

    public void setRevAuthor(Employee revAuthor) {
        this.revAuthor = revAuthor;
    }

    @ManyToOne
    @JoinColumn(nullable = false)
    public Artifact getRevArtifact() {
        return revArtifact;
    }

    public void setRevArtifact(Artifact revArtifact) {
        this.revArtifact = revArtifact;
    }

    @ManyToMany
    @JoinTable(name = "employee_review", joinColumns = @JoinColumn(name = "review_id"), inverseJoinColumns = @JoinColumn(name = "employee_id"))
    public Set<Employee> getRevEmployees() {
        return revEmployees;
    }

    public void setRevEmployees(Set<Employee> revEmployees) {
        this.revEmployees = revEmployees;
    }

    @ManyToMany
    @JoinTable(name = "review_invitations", joinColumns = @JoinColumn(name = "inv_review_id"), inverseJoinColumns = @JoinColumn(name = "inv_employee_id"))
    public Set<Employee> getRevInvitations() {
        return revInvitations;
    }

    public void setRevInvitations(Set<Employee> revInvitations) {
        this.revInvitations = revInvitations;
    }

    @OneToMany(mappedBy="sessReview")
    public Set<Session> getRevSessions() {
        return revSessions;
    }

    public void setRevSessions(Set<Session> revSessions) {
        this.revSessions = revSessions;
    }
}
