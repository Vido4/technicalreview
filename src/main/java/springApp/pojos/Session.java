package springApp.pojos;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Set;

@Entity
public class Session {
    private Long id;
    private String sessStatus;
    private String description;
    private Timestamp start_time;
    private Timestamp end_time;
    private Employee sessAuthor;
    private Artifact sessArtifact;
    private Set<Comment> comments;
    private Set<Employee> sessEmployees;
    private Set<Employee> sessInvitations;
    private Review sessReview;

    @Id
    @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(nullable = false)
    public String getSessStatus() {
        return sessStatus;
    }

    public void setSessStatus(String sessStatus) {
        this.sessStatus = sessStatus;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Column(nullable = false)
    public Timestamp getStart_time() {
        return start_time;
    }

    public void setStart_time(Timestamp start_time) {
        this.start_time = start_time;
    }

    @Column(nullable = false)
    public Timestamp getEnd_time() {
        return end_time;
    }

    public void setEnd_time(Timestamp end_time) {
        this.end_time = end_time;
    }

    @ManyToOne
    @JoinColumn(nullable = false)
    public Employee getSessAuthor() {
        return sessAuthor;
    }

    public void setSessAuthor(Employee author) {
        this.sessAuthor = author;
    }

    @ManyToOne
    @JoinColumn(nullable = false)
    public Artifact getSessArtifact() {
        return sessArtifact;
    }

    public void setSessArtifact(Artifact sessArtifact) {
        this.sessArtifact = sessArtifact;
    }

    @OneToMany(mappedBy = "commSession")
    public Set<Comment> getComments() {
        return comments;
    }

    public void setComments(Set<Comment> comments) {
        this.comments = comments;
    }

    @ManyToMany
    @JoinTable(name = "employee_session", joinColumns = @JoinColumn(name = "session_id"), inverseJoinColumns = @JoinColumn(name = "employee_id"))
    public Set<Employee> getSessEmployees() {
        return sessEmployees;
    }

    public void setSessEmployees(Set<Employee> sessEmployees) {
        this.sessEmployees = sessEmployees;
    }

    @ManyToMany
    @JoinTable(name = "session_invitations", joinColumns = @JoinColumn(name = "inv_session_id"), inverseJoinColumns = @JoinColumn(name = "inv_employee_id"))
    public Set<Employee> getSessInvitations() {
        return sessInvitations;
    }

    public void setSessInvitations(Set<Employee> sessInvitations) {
        this.sessInvitations = sessInvitations;
    }

    @ManyToOne
    @JoinColumn(nullable = false)
    public Review getSessReview() {
        return sessReview;
    }

    public void setSessReview(Review sessReview) {
        this.sessReview = sessReview;
    }

}
