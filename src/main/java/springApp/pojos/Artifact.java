package springApp.pojos;

import javax.persistence.*;
import java.util.Set;

@Entity
public class Artifact {
    private Long id;
    private String source;
    private String description;
    private String type;
    private Set<Session> sessions;
    private Set<Review> reviews;

    public Artifact() {}

    public Artifact(String source, String type) {
        this.source = source;
        this.type = type;
    }

    @Id
    @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(nullable = false)
    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source.trim();
    }

    @Column(nullable = false)
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @OneToMany(mappedBy = "sessArtifact")
    public Set<Session> getSessions() {
        return sessions;
    }

    public void setSessions(Set<Session> sessions) {
        this.sessions = sessions;
    }

    @OneToMany(mappedBy = "revArtifact")
    public Set<Review> getReviews() {
        return reviews;
    }

    public void setReviews(Set<Review> reviews) {
        this.reviews = reviews;
    }
}
