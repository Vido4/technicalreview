package springApp.pojos;

import java.util.Set;

public class SessionResponse {
    private String type;
    private String data;
    private String description;
    private Set<CommentResponse> comments;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<CommentResponse> getComments() {
        return comments;
    }

    public void setComments(Set<CommentResponse> comments) {
        this.comments = comments;
    }
}
