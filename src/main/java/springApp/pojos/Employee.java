package springApp.pojos;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.Set;

@Entity
public class Employee {

    private String login;
    private String email;
    private Set<Role> roles;
    private String password;

    private String name;
    private String surname;
    private String position;
    private String department;
    private Set<Comment> empComments;
    private Set<Session> createdSessions;
    private Set<Session> sessions;
    private Set<Session> sessionInvitations;
    private Set<Review> reviews;
    private Set<Review> reviewInvitations;
    private Set<Review> createdReviews;

    @Id
    @Size(min = 4, max = 24, message = "{login.size}")
    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login.trim();
    }

    @Column(nullable = false)
    @NotBlank(message = "{email.blank}")
    @Email(message = "{email.valid}")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email.trim();
    }

    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "employee_role", joinColumns = @JoinColumn(name = "employee_login"), inverseJoinColumns = @JoinColumn(name = "role_id"))
    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    @JsonIgnore
    @Column(nullable = false)
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password.trim();
    }

    @Column(nullable = false)
    @NotBlank(message = "{name.blank}")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name.trim();
    }

    @Column(nullable = false)
    @NotBlank(message = "{surname.blank}")
    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname.trim();
    }

    @Column(nullable = false)
    @NotBlank(message = "{position.blank}")
    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position.trim();
    }

    @Column(nullable = false)
    @NotBlank(message = "{department.blank}")
    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department.trim();
    }

    @OneToMany(mappedBy = "commAuthor")
    public Set<Comment> getEmpComments() {
        return empComments;
    }

    public void setEmpComments(Set<Comment> empComments) {
        this.empComments = empComments;
    }

    @OneToMany(mappedBy = "sessAuthor")
    public Set<Session> getCreatedSessions() {
        return createdSessions;
    }

    public void setCreatedSessions(Set<Session> createdSessions) {
        this.createdSessions = createdSessions;
    }

    @ManyToMany(mappedBy = "sessEmployees")
    public Set<Session> getSessions() {
        return sessions;
    }

    public void setSessions(Set<Session> sessions) {
        this.sessions = sessions;
    }

    @ManyToMany(mappedBy = "sessInvitations")
    public Set<Session> getSessionInvitations() {
        return sessionInvitations;
    }

    public void setSessionInvitations(Set<Session> sessionInvitations) {
        this.sessionInvitations = sessionInvitations;
    }

    @ManyToMany(mappedBy = "revEmployees")
    public Set<Review> getReviews() {
        return reviews;
    }

    public void setReviews(Set<Review> reviews) {
        this.reviews = reviews;
    }

    @ManyToMany(mappedBy = "revInvitations")
    public Set<Review> getReviewInvitations() {
        return reviewInvitations;
    }

    public void setReviewInvitations(Set<Review> reviewInvitations) {
        this.reviewInvitations = reviewInvitations;
    }

    @OneToMany(mappedBy = "revAuthor")
    public Set<Review> getCreatedReviews() {
        return createdReviews;
    }

    public void setCreatedReviews(Set<Review> createdReviews) {
        this.createdReviews = createdReviews;
    }
}
