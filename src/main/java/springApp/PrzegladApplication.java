package springApp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@EnableAutoConfiguration
@SpringBootApplication
public class PrzegladApplication {

    public static void main(String[] args) {
        SpringApplication.run(PrzegladApplication.class, args);
    }
}
