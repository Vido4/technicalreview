package springApp.storage;

import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface StorageService {
    StorageResponse store(MultipartFile file) throws IOException;
    String getContent(String path, String type);
}