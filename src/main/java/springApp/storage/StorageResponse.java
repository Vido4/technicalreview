package springApp.storage;

import org.springframework.http.HttpStatus;

public class StorageResponse {
    private String message;
    private HttpStatus status;
    private String path;
    private String type;

    public StorageResponse(String message, HttpStatus status) {
        this.message = message;
        this.status = status;
    }

    public StorageResponse(String message, HttpStatus status, String path, String type) {
        this.message = message;
        this.status = status;
        this.path = path;
        this.type = type;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public HttpStatus getStatus() {
        return status;
    }

    public void setStatus(HttpStatus status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
