package springApp.storage;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import springApp.repository.ArtifactRepository;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Service
public class StorageServiceOnLocal implements StorageService {

    private Path rootLocation;

    @Autowired
    private ArtifactRepository artifactRepository;

    @Autowired
    public StorageServiceOnLocal(StorageProperties properties) {
        this.rootLocation = Paths.get(properties.getLocation());
    }

    @Override
    public StorageResponse store(MultipartFile file) throws IOException {
        if (file == null || file.isEmpty()) {
            return new StorageResponse("File is empty",
                    HttpStatus.BAD_REQUEST);
        }

        if (file.getOriginalFilename() == null) {
            return new StorageResponse("Cannot retrieve original filename", HttpStatus.BAD_REQUEST);
        }

        String[] fileFrags = file.getOriginalFilename().split("\\.");
        String extension = fileFrags[fileFrags.length - 1];
        String type;

        if (extension.equalsIgnoreCase("TXT")) {
            type = "TEXT";
        } else if (isImageExtension(extension)) {
            type = "IMAGE";
        } else {
            return new StorageResponse("Wrong file format of " + file.getOriginalFilename(),
                    HttpStatus.BAD_REQUEST);
        }
        return storeOnDisk(file, type);
    }

    @Override
    public String getContent(String path, String type) {
        try {
            if ("TEXT".equalsIgnoreCase(type)) {
                File textFile = new File(path);
                if (textFile.exists()) {
                    InputStream fileInputStream = new FileInputStream(textFile);
                    return IOUtils.toString(fileInputStream, Charset.defaultCharset());
                }
            } else {
                return path;
            }
        } catch (IOException e) {
//            e.printStackTrace();
        }
        return path;
    }

    private boolean isImageExtension(String extension) {
        return extension.equalsIgnoreCase("PNG")
                || extension.equalsIgnoreCase("JPG")
                || extension.equalsIgnoreCase("JPEG")
                || extension.equalsIgnoreCase("GIF")
                || extension.equalsIgnoreCase("BMP");
    }

    private StorageResponse storeOnDisk(MultipartFile file, String type) throws IOException {
        try {
            boolean duplicate = false;
            File directory = new File(rootLocation.toString());
            if (!directory.exists()) {
                if (!directory.mkdirs()) {
                    return new StorageResponse("Cannot create directory" + rootLocation.toString(),
                            HttpStatus.INTERNAL_SERVER_ERROR);
                }
            }
            Path locationPath = this.rootLocation.resolve(file.getOriginalFilename());
            while (Files.exists(locationPath)) {
                if (file.getSize() == Files.size(locationPath)) {
                    duplicate = true;
                    break;
                }
                String randomString = RandomStringUtils.randomAlphanumeric(10, 12);
                locationPath = this.rootLocation.resolve(randomString + file.getOriginalFilename());
            }
            if (!duplicate) {
                Files.copy(file.getInputStream(), locationPath);
            }
            return new StorageResponse("Successfully uploaded file " + file.getOriginalFilename(),
                    HttpStatus.OK, locationPath.toString(), type);
        } catch (IOException e) {
            return new StorageResponse(e.toString(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
