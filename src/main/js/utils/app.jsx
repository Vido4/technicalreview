import React from 'react';
import { Route, Switch } from 'react-router-dom';

import NewSession from 'pages/NewSession';
import UserAccount from 'pages/UserAccount';

import Container from 'components/Container';

const App = () => (
  <Container>
    <Switch>
      <Route source="/" exact component={UserAccount} />
      <Route source="/new-session" component={NewSession} />
    </Switch>
  </Container>
);

export default App;
