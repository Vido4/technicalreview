import React from 'react';
import styled, { css } from 'styled-components';
import axios from 'axios';

import Image from 'components/Artifacts/image';
import Text from 'components/Artifacts/text';
import Comments from 'components/Comments';
import Button from 'components/Button';

const Container = styled.div`
  width: 1120px;
  margin: 0 auto;
`;

const ArtifactWrapper = styled.div`
  background: pink;
  min-height: 100px;
  width: 100%;
  margin-bottom: 20px;
`;

const DescriptionWrapper = styled.div`
  font-style: italic;
  margin-top: 10px;
  padding-left: 10px;
  margin-bottom: 30px;
  padding-bottom: 20px;
  border-bottom: 1px solid black;
`;

const NewCommentWrapper = styled.div`
  margin-top: 50px;
  margin-bottom: 15px;
  display: none;

  ${(props) => props.visible && css`
    display: block;
  `};
`;

const NewCommentInput = styled.textarea`
  width: 900px;
  min-height: 150px;
  display: block;
`;

const XBtn = styled.div`
  cursor: pointer;
  font-size: 35px;
`;

const FlexDiv = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 30px;
`;

class Layout extends React.Component {
  constructor(props) {
    super(props);
    console.log(this.props.data);
    this.state = {
      visibleInput: false,
      newComment: '',
      data: this.props.data.data,
    };
  }

  onNewCommentInputChange(event) {
    this.setState({ newComment: event.target.value });
  }

  onXBtnClick() {
    this.setState({ visibleInput: false });
  }

  onSendBtnClick() {
    axios.post(`/api/session/${this.props.id}/comment`, {
      data: this.state.newComment,
      date: new Date().toISOString(),
    }).then((response) => {
      this.setState({ data: response.data });
    }).then((error) => { console.log(error); });
  }

  newCommentBtnClick() {
    this.setState({ visibleInput: true });
  }

  render() {
    const artifact = this.state.data.type === 'image' ?
      <Image url={this.state.data.data} /> : <Text data={this.state.data.data} />;
    return (
      <Container>
        <ArtifactWrapper>
          {artifact}
        </ArtifactWrapper>
        Description:
        <DescriptionWrapper>
          {this.state.data.description}
        </DescriptionWrapper>
        <div>
          <Comments comments={this.state.data.comments} />
        </div>
        <NewCommentWrapper visible={this.state.visibleInput}>
          <FlexDiv>
            <div>
              Comment:
              <NewCommentInput onChange={(e) => { this.onNewCommentInputChange(e); }} />
            </div>
            <XBtn onClick={() => { this.onXBtnClick(); }}>&#10005;</XBtn>
          </FlexDiv>
          <Button onClick={() => { this.onSendBtnClick(); }}>Send</Button>
        </NewCommentWrapper>
        <NewCommentWrapper visible={!this.state.visibleInput}>
          <Button onClick={() => { this.newCommentBtnClick(); }}>Add new comment</Button>
        </NewCommentWrapper>
        <Button onClick={() => { window.location.href = 'http://localhost:8080/'; }}>Back to index</Button>
      </Container>
    );
  }
}

export default Layout;
