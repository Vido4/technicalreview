import React from 'react';
import axios from 'axios';

import Layout from './layout';

class App extends React.Component {
  constructor(props) {
    super(props);

    const res = window.location.pathname.match(/^\/session\/(\d+)/);
    if (res.length === 2) {
      this.id = res[1];
    }

    this.state = {
      data: null,
    };
  }

  componentDidMount() {
    axios.get(`/api/session/${this.id}`)
      .then((response) => {
        this.setState({ data: response });
      });
  }

  render() {
    return (
      <div>
        {this.state.data ? <Layout data={this.state.data} id={this.id} /> : null};
      </div>
    );
  }
}

export default App;
