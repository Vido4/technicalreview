import React from 'react';
import styled from 'styled-components';

const Text = ({ data }) => (
  <p>
    {data}
  </p>
);

export default Text;
