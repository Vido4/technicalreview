import React from 'react';
import styled from 'styled-components';

const Container = styled.div`
  text-align:center;
`;

const ImgDiv = styled.img`
  max-width: 500px;
  max-height: 400px;
`;

const Image = ({ url }) => (
  <Container>
    <ImgDiv srcSet={`http://localhost:8080/${url}`} alt="artifact" />
  </Container>
);

export default Image;
