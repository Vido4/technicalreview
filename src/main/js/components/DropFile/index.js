import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Dropzone from 'react-dropzone';
import axios from 'axios';

const Container = styled.div`
  .dropzone {
    width: 100%;
    height: 100px;
    border-width: 2px;
    border-color: rgb(102, 102, 102);
    border-style: dashed;
    border-radius: 5px;
    display: flex;
    align-items: center;
    justify-content: center;
  }
`;

class DropFile extends React.Component {
  constructor() {
    super();
    this.state = { files: [] };
  }

  onDrop(files) {
    this.setState({
      files,
    });

    const data = new FormData();

    data.append('artifact', files);


    const config = {
      headers: { 'content-type': 'multipart/form-data' },
    };

    return axios.post('http://localhost:8080/artifacts', data);
  }

  render() {
    return (
      <Container>
        <Dropzone className="dropzone" onDrop={(files) => this.onDrop(files)}>
          <p>Try dropping some files here, or click to select files to upload.</p>
        </Dropzone>
        <div>
          <h4>Dropped files</h4>
          <ul>
            {
              this.state.files.map((el) => <li key={el.name}>{el.name} - {el.size} bytes</li>)
            }
          </ul>
        </div>
      </Container>
    );
  }
}

export default DropFile;
