import React from 'react';
import styled from 'styled-components';

const Container = styled.div`
  width: 100%;
`;

const CommentWrapper = styled.div`
  margin-bottom: 10px;
  border-radius: 5px;
  border: 1px solid black;
`;

const Header = styled.div`
  padding: 5px;
  display: flex;
  justify-content: space-between;
  background: #28a745;
`;

const AuthorText = styled.p`
  color: #fff;
`;

const CommentText = styled.div`
  padding: 5px;
`;

const DateWrapper = styled.div`

`;

const Comments = ({ comments }) => {
  const items = comments.sort((a, b) => {
    console.log(new Date(a.date), new Date(b.date));
    return new Date(a.date) - new Date(b.date);
  });
  console.log(comments, items);  
  return (
    <Container>
      {
        items.map((el, key) => (
          <CommentWrapper key={key}>
            <Header>
              <AuthorText>{el.user}</AuthorText>
              <DateWrapper>{new Date(el.date).toLocaleString()}</DateWrapper>
            </Header>
            <CommentText>
              {el.data}
            </CommentText>
          </CommentWrapper>
        ))
      }
    </Container>
  );
}
export default Comments;
