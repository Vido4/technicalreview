import React from 'react';
import PropTypes from 'prop-types';
import styled, { ThemeProvider } from 'styled-components';
import theme from '../../theme';

const StyledContainer = styled.div`
  width: ${(props) => props.theme.container.width};
  margin: 0 auto;
`;

const Container = ({ children }) => (
  <ThemeProvider theme={theme}>
    <StyledContainer>
      {children}
    </StyledContainer>
  </ThemeProvider>
);

Container.propTypes = {
  children: PropTypes.node.isRequired,
};

export default Container;
