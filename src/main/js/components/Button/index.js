import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const StyledButton = styled.button`
  color: #fff;
  font-size: 16px;
  width: 100%;
  border-radius: 6px;
  background-size: 110% 110%;
  background-position: -1px -1px;
  background-color: #28a745;
  background-image: linear-gradient(-180deg, #34d058 0%, #28a745 90%);
  border: 1px solid rgba(27,31,35,0.2);
  padding: 0.75em 1.25em;
  cursor: pointer;
`;

const Button = ({ onClick, children }) => (
  <StyledButton onClick={onClick}>
    {children}
  </StyledButton>
);

Button.propTypes = {
  children: PropTypes.oneOfType([PropTypes.node, PropTypes.string]).isRequired,
  onClick: PropTypes.func,
};

Button.defaultProps = {
  onClick: () => {},
};

export default Button;
